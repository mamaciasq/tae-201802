Este repositorio tiene información relacionada con el curso Tópicos avanzados en Estadística de la Maestría en Estdística de la Universidad Nacional de Colombia.

El profesor que dicta el curso es [Santiago Velasco-Forero](http://cmm.ensmp.fr/~velasco/index.html).

Las sesiones están organizadas de la siguiente forma:

+ Sesión 1 : Introducción a la Computación Estadística.
  Taller 0: Python, Jupyter, Numpy, etc.  
  [Tutorial0a](https://gitlab.com/mamaciasq/tae-201802/blob/master/Tutorial0aPython.ipynb)  
  [Tutorial0b (TAREA 1)](https://gitlab.com/mamaciasq/tae-201802/blob/master/Tutorial0bNumpy.ipynb)  
  [Tutorial0c](https://gitlab.com/mamaciasq/tae-201802/blob/master/Tutorial0cMatplotlib.ipynb)  
  [Tutorial0d](https://gitlab.com/mamaciasq/tae-201802/blob/master/Tutorial0dPandas.ipynb)

+ Sesión 2 : Estimación Densidad No-paramétrica y Efectos de la Alta Dimensión. 
 Taller 1: Métodos Estimación de Densidad  
 Taller 2: Maldición de la Alta Dimensión 

+ Sesión 3 : Principios de la Estimación Robusta  
 Taller 3: Estimación Robusta de matriz de covarianza.  
 Taller 4: Estimación Empírica del Punto de Ruptura de un método.  

+ Sesión 4 : Algoritmos de Expectación y Maximización.  
 Taller 5: EM en práctica.

+ Sesión 5 : Análisis Estadístico “on Streaming”  
 Taller 6: Aplicando SMS en análisis de datos. (Simple Matrix Sketches)
  
+ Examen 1:

+ Sesión 6 : Introducción a la clasificación, Regresión Regularizada y el Análisis Discriminante.  
 Taller 7: Scikit-learn / LDA

+ Sesión 7 : Arboles de Regresión y Clasificación. Métodos de ensamblaje.  
 Taller 8: CART / Adaboost

+ Sesión 8 : Métodos de descenso de gradiente y la Regresión Profunda.  
 Taller 9: Keras

+ Sesión 9 : Del re-muestreo a la aumentación de datos.  
 Taller 10: Keras

+ Sesión 10 : Examen 2

+ Sesión 11: Introducción ANS / Grafos, Matrices “Sparse” y sus aplicaciones.  
 Taller 11: De datos a grafos y uso de las matrices “Sparse”.

+ Sesión 12: Agrupamiento con Grafos.   
 Taller 12: Clustering usando grafos.

+ Sesión 13: Agrupamiento Jerárquico: Un perspectiva basada en grafos.  
 Taller 13: Clustering jerárquico usando grafos.

+ Sesión 14: Agrupamiento y Persistencia.  
 Taller 14: Persistencia en Clustering jerárquico.

+ Sesión 15: Examen Final


