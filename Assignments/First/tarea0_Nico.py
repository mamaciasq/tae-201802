#################################################################################
#################################### TAREA 1 ####################################
#################################################################################

import numpy as np

# Escriba una función para cada uno de los siguiente problemas:
# 1. Considere dos arreglos A y B de la misma forma. Escriba una función para determinar si A y B son iguales.
# In[1]


# In[1a]. Datos de ejemplo para evaluación de la igualdad
data1 = [1, 2, 3, 4, 5]
data2 = [range(1, 5), range(5, 9)] 
A = np.array(data1)     # Un arreglo unidimensional
B = np.array(data2)     # Un arreglo bi-dimensional
C = np.array(data2)     # Un arreglo bi-dimensional
D = np.array(data2) + 1 # Un arreglo bi-dimensional

print(A.ndim)
print(B.ndim)
print(C.ndim)

print(A.shape)
print(B.shape)
print(C.shape)

# In[1b]. Función para evaluar la igualdad entre los arreglos

def igualdad(x,y):
    if x.ndim==y.ndim:
        if x.shape==y.shape and (x==y).all():
            print("Dimensiones y entradas iguales")
        else: 
            print("Dimensiones iguales pero entradas diferentes")
    else:
        print("Dimensiones diferentes") 

igualdad(A,B)
igualdad(B,C)
igualdad(C,D)

# 2. Cree un vector aleatorio de dimension 10 y reemplace el máximo valor por cero.
# In[2]
np.random.seed(1)
rnd10 = np.random.randn(10) 
rnd10[rnd10.argmax()]=0  


# 3. X=np.random.uniform(0,100). Encuentrar el índice con el valor más cercano a un número dado.
# In[3]
np.random.seed(1)
X=np.random.uniform(0,100,100)

def indice(num_dado):
    abs_d =abs(X-num_dado)
    print(abs_d.argmin())
    
indice(72)
indice(42)
    
# 4. X = np.random.random((100,2)). Encontrar la distance entre todo par de puntos, (una matriz (100,100)).
# In[4]
np.random.seed(1)
X = np.random.random((100,2))
D = np.zeros((100, 100))

for i in range(X.shape[0]):
    for j in range(X.shape[0]):
        D[i,j]= np.sqrt(np.sum((X[i, :] - X[j, :])**2))        

# 5. X= np.random.random((100,10)). Substraer el promedio de cada columna de una matriz.
# In[5]
np.random.seed(1)
X= np.random.random((100,10))
Xtot=X.sum(axis=0)
Xbar=Xtot/X.shape[0]

# 6. X = np.random.uniform(0,1,10) y z=.2. Calcular el valor en X que sea el más cercano a z.
# In[6]
np.random.seed(1)
X = np.random.uniform(0,1,10)
z = .2
X[abs(X-z).argmin()]