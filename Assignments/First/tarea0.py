# Tarea 1

# Escriba una función para cada uno de los siguiente problemas:

import math
import numpy as np

# 1. Considere dos arreglos A y B de la misma forma. Escriba una función para determinar si A y B son iguales.

A = np.array([[0, 1, 2, 3], [0, 1, 2, 3]]); print(A)
B = np.arange(8, dtype = int).reshape((2, 4)); print(B)
for i in range(len(A.flatten())):
      print(A.flatten()[i] == B.flatten()[i])
      
# 2. Cree un vector aleatorio de dimension 10 y reemplace el máximo valor por cero.

vec=np.random.randn(10); vec
vec[vec.argmax()]=0; vec

# 3. X=np.random.uniform(0,100). Encontrar el índice con el valor más cercano a un número dado.

X = np.random.uniform(0, 100, 5); X
np.random.seed(12234)
index = np.arange(5)
W = np.arange(25).reshape(5, 5)

for i in range(len(X)):
    for j in range(len(index)):
        W[i][j]=abs(X[i]-index[j])

W

# 4. X = np.random.random((100,2)). Encontrar la distancia entre todo par de puntos, (una matriz (100,100)).

X = np.random.random((100, 2))
dist=np.zeros((100,100))
for i in range(100):
    for j in range(100):
        dist[i][j] = np.sqrt(np.sum((X[i,:] - X[j,:]) ** 2))
dist



# 5. X = np.random.uniform(0,1,10) y z=.2. Calcular el valor en X que sea el más cercano a z.

X= np.random.random((100, 10)); X
Z=np.zeros(10)
for j in range(len(Z)):
    Z[j]=X[:,j].mean()
    Z.tolist()
Z

# 6. X= np.random.random((100,10)). Sustraer el promedio de cada columna de una matriz.

X = np.random.uniform(0, 1, 10) 
np.random.seed(12234)
z = 0.2
Y = list([0,1,2,3,4,5,6,7,8,9])
for i in range(len(Y)):
    Y[i]=abs(X[i]-z)
    
a=sorted(Y).pop(0)
a+z

# 7 Dado:

A=np.arange(10)
B=np.arange(10)
np.random.shuffle(B)

np.einsum('i->', A)  
sum(A)

np.einsum('i,i->i', A, B)
A * B

np.einsum('i,i', A, B)
np.inner(A, B)

np.einsum('i,j', A, B)
np.outer(A, B)

# 8.) En el caso de matrices, a que equivalen la siguiente operaciones

A=np.random.rand(10,10)
B=np.random.rand(10,10)
# A y B son Matrices
np.einsum('ij', A)
# La función equivalente es A       

np.einsum('ji', B)
# La función equivalente es B.T       

np.einsum('ii->i', A)
# La función equivalente es diag(A)       

np.einsum('ij,jk', A, B)
# La función equivalente es dot(A, B)      

np.einsum('ij->', A)
# La función equivalente es sum(A)   

np.einsum('ij->i', A)
# La función equivalente es sum(A, axis=1)   