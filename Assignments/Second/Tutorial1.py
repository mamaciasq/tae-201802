# Ajustando distribuciones multivariadas

# Cargando librerías
from scipy import stats  
import numpy as np  
import matplotlib.pylab as plt
import warnings
warnings.filterwarnings("ignore")

# Función AIC
def AIC(distr, param, data):
    """Calcula el criterio de información de Akaike"""
    """https://es.wikipedia.org/wiki/Criterio_de_informaci%C3%B3n_de_Akaike"""
    return 2*len(param)-2*np.sum(distr.logpdf(data, param[0], param[1], * param[2:]))

# Número de datos
n = 1000 

# Crear datos de una distribucion normal con ruido
data = np.random.normal(0, 2, n)

#Carga todas la posible distribuciones que tienen la posibilidad de usar "fit"
distrlist=[]
for this in dir(stats):
    if "fit" in eval("dir(stats." + this +")"):
        distrlist.append(this)
print(distrlist)

listAIC=[]
for distrName in distrlist:
    distr = getattr(stats.distributions, distrName)
    try:  
        print('Distribución:',distrName)
        param = distr.fit(data)
        pdf = distr.pdf(lnspc, *param)
        plt.plot(lnspc, pdf, label=distrName)
        print('Número de parámetros: ',len(param))
        res=AIC(distr,param,data)
        if res<0:
            res=np.Inf
        listAIC.append(res)
        print('AIC: ',res)
        print('*'*50)
    except:
        print('Error')
        listAIC.append(np.Inf)
        
# A simple program

x = int(input('Ingrese un entero: '))

if x % 2 == 0:
    #print(`´)
    print('Even')
else:
    #print(`´)
    print('Odd')
    
print('Done with conditional')

# Nested conditionals

if x % 2 == 0:
    if x % 3 == 0:
        print('Divisible por 2 y 3')
    else:
        print('Divisible por 2 pero no por 3')
elif x % 3 == 0:
    print('Disible por 3 pero no por 2')

# Compound booleans
    
if x < y and x < z:
    print('x es el menor')
elif y < z:
    print('y es el menor')
else: 
    print('z es el menor')