# Maldición de la dimensionalidad


# Este tutorial ilusta lo que de llama la "maldición de la alta dimensión". 
# Por ejemplo, generando puntos uniformemente en el hipercubo unitario, 
# es fácil del ver que la distancia promedio entre estos puntos aleatorios, 
# aumenta con la dimensión. De la misma manera, dichos puntos tienden a caer 
# fuera de la hiperesfera unitaria inscrita en el hypercubo, mostrando que en 
# alta dimensionaes, casi todo el volumen del espacio se encuentra en las 
# esquinas del hipercubo.

# 1. Norma Euclideana y Distancia

# Norma Euclideana

# Considere un vector $y=(y_1,…,y_n)$ con $n$ elementos. The norma Euclidean 
# de un vector $y$ es denotada por $||y|| $, y definida por la versión 
# generalizada del teorema de Pytagoras,

# $$ \vert\vert{y}\vert\vert = \sqrt{y_1^2 + y_2^2 + ..... y_n^2} $$

# En otras palabras, la norma del vector es la raiz cuadrada de la suma de los 
# elementos al cuadrado.

# Adicionalmente, es fácil de ver que viendo $y$ como un vector fila, entonces 
# la raiz cuadrada del producto punto por el mismo es exactamente su norma 
# Euclideana, es decir:
# $$\vert\vert{y}\vert\vert = \sqrt{yy^T} $$


# Como calcular la norma Euclideana de un vector en Python

import numpy as np
def norma_euclideana(x): return np.linalg.norm(x)


# Usando Python numpy, uno puede calcular rapidamente  la distancia Euclideana 
# por medio de la norma del vector, es decir np.linalg.norm. Podemos probar en 
# un caso simple:
# ||(3,4)||=5

norma_euclideana(np.array([3, 4]))


# Distancia Euclideana

# La distancia Euclideana entre dos vectores $ x=(x_1,…,x_n)$ y $y=(y_1,…,y_n)$, 
# denotada por $ d(x,y) $ es la normal Euclideana del vector $ x−y $ es decir,

# $$ d(x,y) = \sqrt{(x_1 - y_1)^2 +.....(X_n - y_n)^2} $$

x = np.array([3, 4])
y = np.array([2, 3])

distancia_euclideana = norma_euclideana(x-y)
distancia_euclideana


# Otra manera de calcular esta distancia Euclideana es usando la librería 
# scipy.spatial.distance 

from scipy.spatial.distance import euclidean

euclidean(x,y)


# 2. Todo el Volumen se encuentra en las esquinas

# Asuma que se tiene un cuadrado con un circulo inscrito en el, o en general 
# un hiper-cubo y una hiper-esfera. En esta sección ilustramos el curioso hecho 
# que cuando la dimensión crece, la mayor parte de los puntos en el hipercubo 
# caen fuera de la hiperespera.

# ### Usando el método de Monte Carlo para calcular el valor de $\pi$

# Vamos a mirar el caso donde $d=2$. Se sabe que el area del circulo es $\pi r^2$, 
# con $r=\frac{1}{2}$, así que el area es $\frac{\pi}{4}$. Por el método de 
# MonteCarlo, se generan valores aleatorios en el cuadrado y se cuentan cuantos 
# valores están en el circulo.

import matplotlib.pyplot as plt

M = int(1e2)
dim=2
y = np.random.uniform(low=-0.5, high=0.5,size=(M,dim))
plt.plot(y[:,0], y[:,1], '.')
plt.show()
print(y.shape)

p = np.sum(np.sqrt(y[:,0]**2 + y[:,1]**2) < 0.5)/M
print('Estimación de Pi:',4*p)
print('Error de la estimación',abs(4*p-np.pi))

# Ahora aumentando el número de simulaciones, la aproximación mejora.

M = int(1e3)
#print(M)
y = np.random.uniform(low=-0.5, high=0.5,size=(M,dim))
p = np.sum(np.sqrt(y[:,0]**2 + y[:,1]**2) < 0.5)/M
print(4*p)
print('Error de aproximación:',abs(4*p-np.pi))

M = int(1e5)
#print(M)
y = np.random.uniform(low=-0.5, high=0.5,size=(M,dim))
p = np.sum(np.sqrt(y[:,0]**2 + y[:,1]**2) < 0.5)/M
print(4*p)
print('Error de aproximación:',abs(4*p-np.pi))

M = int(1e7)
#print(M)
y = np.random.uniform(low=-0.5, high=0.5,size=(M,dim))
p = np.sum(np.sqrt(y[:,0]**2 + y[:,1]**2) < 0.5)/M
print(4*p)
print('Error de aproximación',abs(4*p-np.pi))


# Ahora, vamos a calcular el volumen de la hiperesfera inscrita en el hipercubo 
# unitario (el cual tiene volumen uno) aumentando la dimensions.

from scipy.spatial.distance import cdist
import scipy
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rcParams['figure.figsize'] = [12,10]
plt.style.use('seaborn-darkgrid')

dim_MAX = 20
M = int(1e5)
dims = np.zeros(dim_MAX, dtype=np.int32)
volume = np.zeros(dim_MAX)
for dim in range(1,dim_MAX+1):
    y = np.random.uniform(low=-0.5, high=0.5,size=(M,dim))
    # cdist calcula la distancia entre dos matrices incluso de diferente tamaño y-> (M,N) and [[0..0]]
    dist = cdist(y,np.expand_dims(np.zeros(dim),0),metric='euclidean')
    dims[dim-1] = dim
    volume[dim-1] = np.sum(dist < 0.5)/M
df=pd.DataFrame(data={'dims': dims,'volume': volume})
print(df)


# Así hemos calculado la probabilidad que un vector esté en la hiperesfera 
# inscrita en la hipercubo unitario, esa cantidad da el volumen de dicha hiperesfera.

plt.plot(df.dims, df.volume,'o-')
plt.title('Volumen de la Hyperbola inscrita en el hypercubo',size=18)
plt.xlabel('Dimensión',size=14)
plt.ylabel('Volumen de la hyperbola inscrita',size=14)


# Todos los puntos estan lejos!

# Otra manera de ver el efecto de la alta dimensión, es que los datos están en 
# promedio cada vez más lejos. 

# Caso: Distribución Uniforme

M=100
for rep in range(2): #Número de Repeticiones del Experimento
    resmin=[]
    resmax=[]
    resmean=[]
    for dim in range(500):
        y = np.random.uniform(low=-.5, high=0.5,size=(M,dim+1))
        dist = cdist(y,y,metric='euclidean')
        v=dist[np.triu_indices(M, k = 1)]
        resmin.append(np.min(v))
        resmax.append(np.max(v))
        resmean.append(np.mean(v))
    plt.plot(resmin,label='Min')
    plt.plot(resmean,label='Mean')
    plt.plot(resmax,label='Max')
plt.xlabel('Dimension')
plt.ylabel('Distancia Euclideana')


# Caso: Distribución Gaussiana

M=100
for rep in range(2):
    resmin=[]
    resmax=[]
    resmean=[]
    for dim in range(500):
        y = np.random.normal(size=(M,dim+1))
        dist = cdist(y,y,metric='euclidean')
        v=dist[np.triu_indices(M, k = 1)]
        resmin.append(np.min(v))
        resmax.append(np.max(v))
        resmean.append(np.mean(v))
    plt.plot(resmin,label='Min')
    plt.plot(resmean,label='Mean')
    plt.plot(resmax,label='Max')
#plt.legend()
plt.xlabel('Dimension')
plt.ylabel('Distancia')


# Caso: Correlación en Alta Dimensión

M=100
for rep in range(2):
    resmin=[]
    resmax=[]
    resmean=[]
    for dim in range(500):
        y = np.random.normal(size=(M,dim+1))
        dist = cdist(y,y,metric='correlation')
        #Cdist calcular el valor 1 - CORR según la documentación
        #https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.cdist.html
        
        v=dist[np.triu_indices(M, k = 1)]-1
        resmin.append(np.min(v))
        resmax.append(np.max(v))
        resmean.append(np.mean(v))
    plt.plot(resmin,label='Min')
    plt.plot(resmean,label='Mean')
    plt.plot(resmax,label='Max')
#plt.legend()
plt.xlabel('Dimension')
plt.ylabel('Correlación')
plt.show()


# ## 4. Distribución Normal

# El logaritmo de la distribución de normal con parámetros $\mu \in \mathbb{R}$ 
# y $\sigma \in \mathbb{R}^+$ es definito por 
 
# $$ \mathsf{Normal}(y \mid \mu, \sigma) = \frac{1}{\sqrt{2 \pi}}\frac{1}{\sigma} \exp \left( -\frac{1}{2} \left( \frac{y - \mu}{\sigma} \right)^2 \right). $$
 
# Para el caso de distribución estandarizada: 
 
# $$ \mathsf{Normal}(y \mid 0, 1) = \frac{1}{\sqrt{2 \pi}} \exp \left( -\frac{1}{2} \ y^2 \right). $$
 
# Luego de cambiar a escala logaritmica, se tiene:
 
# $$ \log \mathsf{Normal}(y \mid 0, 1) \ = \ -\frac{1}{2} \ y^2 + \mathrm{constante} $$
 
# donde la constante no depende $y$. 
 
# De esta forma, es fácil de ver la relación entre la distribución normal y la distance.
 
# Para multiples muestras i.i.d. con distribución normal estandar $y = (y_1, \ldots, y_N)$, y 
# $$ \log p(y)  = \sum_{n=1}^N -\frac{1}{2} y_n^2 + c =-\frac{1}{2} yy^{\top} +c = -\frac{1}{2} || y ||^2 + c$$
# En otras palabras, el log de la densidad en esta caso es proporcional a la 
# mitad de la distancia al origen. Un desarrollo similar puede hacerse para el 
# caso multivariado.

# 5. Vector Aleatorio Normal Unitario

D = 10
u = np.random.randn(D)
print(u)

print(norma_euclideana(u))


# Cual es la distribución de la norma de los vectores en función de la dimensión?

import scipy.stats as stats

N = int(1e4)
dim = np.array([1, 2, 4, 5, 8, 11, 16, 22, 32, 45, 64, 90, 128, 181, 256])
D = len(dim)

lower = np.zeros(D);
middle = np.zeros(D);
upper = np.zeros(D);
lower_ll = np.zeros(D);
middle_ll = np.zeros(D);
upper_ll = np.zeros(D);
max_ll = np.zeros(D);

for k in range(D):
    d = dim[k]
    y = np.zeros(N)
    ll = np.zeros(N)

    for n in range(N):
        sim=np.random.randn(d)
        y[n] = norma_euclideana(sim)
        ll[n] = np.sum(stats.norm.logpdf(sim))
        
    # Intervalos de la norma euclidiana        
    lower[k] = np.percentile(y,.5)
    middle[k] = np.percentile(y,50)
    upper[k] = np.percentile(y,99.5)
    
    # Intervalos de confianza de la verosimilitud
    lower_ll[k] = np.percentile(ll,.5)
    middle_ll[k] = np.percentile(ll,50)
    upper_ll[k] = np.percentile(ll,99.5)
    
    max_ll[k] = np.sum(stats.norm.logpdf(np.zeros(d))) 
    #La moda de la distribución es el vector O

from collections import OrderedDict
df = pd.DataFrame(OrderedDict({'dim':dim, 'lb':lower, 'mid': middle, 'ub': upper,
                      'lb_ll' : lower_ll, 'mid_ll': middle_ll,
                      'ub_ll' : upper_ll, 'max_ll' : max_ll}))
df


plt.plot(df.mid,'o-')
plt.fill_between(np.arange(len(df)),df.lb, df.ub,color='yellow', alpha=0.2)
plt.xticks(np.arange(len(df)), df.dim)
plt.xlabel('Número de Dimensiones',size=14)
plt.ylabel('Distancia Euclideana desde el Origen',size=14)
plt.title('IC de 99%', size=18)
plt.show()


# Incluso en dimensiones 16, el intervalo de 99% de confianza está muy lejos 
# de cero, el cual es la moda de la dsitribución.

# Medida de Concentración

# El siguiente gráfico muestra el valor mediano del logaritmo de la probabilidad 
# y el IC del 99% de los valores aleatorios


plt.plot(df.mid_ll,'o-')
plt.plot(df.max_ll,'ro-')
plt.fill_between(np.arange(len(df)),df.lb_ll, df.ub_ll,color='yellow', alpha=0.2)
plt.xticks(np.arange(len(df)), df.dim)
plt.xlabel('Número de Dimensiones',size=14)
plt.ylabel('Log Densidad',size=14)
plt.title('Fig 1: Mediana e Intervalos de 99% de confianza del log densidad para datos aleatorios \n y comparación con la moda de la distribución', size=18)
plt.show()


# ### Nadie es promedio!
 
# Esto es contraintruitivo, porque se puede concluir que el individuo promedio 
# de la población está fuera del intervalo de confianza, es decir que es un 
# individuo "outlier". Porque un individuo que tiene cada caracteristica promedio 
# es inusual? Precisamente, porque es inusual de tener tantas variables cercanas 
# al promedio.
 

# Tarea:

# Cree una función que genere 1000 vectores aleatorios de una distribución 
# normal multivariada con vector media $\mu=0_{d}$ y matrix de covarianza 
# $2*I_{d}$, para diferente valores de d (dimension).
 
# a) Calcule y grafique los intervalos de confianza de 95% para las distancias 
# entre los vectores generados.
 
# b) Calcule y grafique los intervalos de confianza de 95% para el ángulo entre 
# los vectores generados.
 
# c) Con una vector media $\mu=2_{d}$ y matrix de covarianza $2*I_{d}$. Realice 
# la Fig. 1 (Mediana e Intervalos de 99%...) para este caso. (Note que se debe 
# adaptar el código en la simulación y en el cálculo de la probabilidad asociada 
# a la moda de la distribución). 
 
# Ayuda:
# La función numpy.random.multivariate_normal permite generar directamente 
# vectores para esta distribución. Por ejemplo: 
# np.random.multivariate_normal(np.zeros(3),np.eye(3),size=5) 
# genera 5 valores aleatorios de la distribución centrada en zero y con matriz identidad.
