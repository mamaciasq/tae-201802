# Tutorial 1: Ajustando distribuciones (univariadas)
# Por Santiago Velasco-Forero
# email: velasco@cmm.ensmp.fr
 
# Definición del Criterio de información de Akaike:

# $AIC(Model,Data)= 2k - 2 \ln(\mathcal{L(Model,Data)})$ donde $\mathcal{L}$ 
# es el máximo valor de la función de verosimilitud para el modelo estimado y 
# $k$ el número de parámetros del modelo.

from scipy import stats  
import numpy as np  
import matplotlib.pylab as plt
import warnings
warnings.filterwarnings("ignore")

def AIC(distr, param, data):
    """Calcula el criterio de información de Akaike"""
    """https://es.wikipedia.org/wiki/Criterio_de_informaci%C3%B3n_de_Akaike"""
    return 2*len(param)-2*np.sum(distr.logpdf(data, param[0], param[1], * param[2:]))

n = 1000

#data1 = np.random.normal(10, 1, n)
#data2 = np.random.normal(5, 1, 100)
#data=np.concatenate([data1,data2])
#data = abs(np.random.normal(0, 2, n))

# Crear datos de una distribucion normal con ruido
data = np.random.normal(0, 2, n)

# Histograma de los datos

plt.hist(data, normed = True)
plt.plot(data, np.zeros(data.shape[0]) - 0.01, '+k')
plt.show()

# Solamente para visualización. Valores para dibujar la distribución teórica
xmin = -6
xmax = 6
lnspc = np.linspace(xmin, xmax, len(data))


distrNameList = ['beta', 'gamma', 'norm', 'laplace', 'johnsonsb', 'johnsonsu']

for distrName in distrNameList:
    distr = getattr(stats.distributions, distrName)
    param = distr.fit(data)
    pdf = distr.pdf(lnspc, *param)
    plt.plot(lnspc, pdf, label=distrName)
    print('Distribución:',distrName)
    print('Número de parámetros: ',len(param))
    print('AIC: ',AIC(distr,param,data))
    print('*'*50)
                
plt.legend()                 
plt.show()
                 
                


#Carga todas las posibles distribuciones que tienen la posibilidad de usar "fit"
distrlist=[]
for this in dir(stats):
    if "fit" in eval("dir(stats." + this +")"):
        distrlist.append(this)
print(distrlist)



listAIC=[]
for distrName in distrlist:
    distr = getattr(stats.distributions, distrName)
    try:  
        print('Distribución:',distrName)
        param = distr.fit(data)
        pdf = distr.pdf(lnspc, *param)
        plt.plot(lnspc, pdf, label=distrName)
        print('Número de parámetros: ',len(param))
        res=AIC(distr,param,data)
        if res<0:
            res=np.Inf
        listAIC.append(res)
        print('AIC: ',res)
        print('*'*50)
    except:
        print('Error')
        listAIC.append(np.Inf)


plt.plot(listAIC,'.')
plt.ylim([min(listAIC)*.95,min(listAIC)*2])
plt.ylabel('AIC')
plt.xlabel('Distribución')
plt.show()



bestDist=np.array(distrlist)[np.argsort(np.array(listAIC))[0:10]]
bestDist


plt.hist(data, normed=True)

distrNameList = list(bestDist)

for distrName in distrNameList:
    distr = getattr(stats.distributions, distrName)
    param = distr.fit(data)
    pdf = distr.pdf(lnspc, *param)
    plt.plot(lnspc, pdf, label=distrName)
    print('Distribución:',distrName)
    print('Número de parámetros: ',len(param))
    print('AIC: ',AIC(distr,param,data))
    print('*'*50)
                
plt.legend()                 
plt.show()


# ### Homework

# A. Los resultados de este experimento dependen de los datos generados. 
# Repita el experimento  $n=1000$, $\mathcal{N}(\mu=10$, $\sigma=1$), 
# 250 veces y describa cuales son las distribuciones que mejor ajustan 
# en el sentido de tener más bajo y menor varianza en el AIC.
 
# B. Repita A para más bajo BIC (Criterio de informacion Bayesiana). 
# $BIC(Model,Data)= \ln(n)k - 2 \ln(\mathcal{L(Model,Data)})$
 
# C. Realice (A) (Se debe mantener $n=1000$), incluyendo datos contaminados 
# de una normal con parametros ($\mu=$5,$\sigma$=1):
 
#     a) (1%) 
     
#     b) (5%)
     
#     c) (10%)
     
# Cuales son las distribuciones que mejor ajustan en el sentido de tener 
# más bajo y menor varianza en el AIC?

# D. Analice la robustes del ajuste de las distribuciones por medio del AIC. 
# Es decir, compare la diferencia de AIC para distribuciones con/sin 
# contaminaciones, usando el mismo tipo de simulaciones en 2. 
 
# E. Repita (A-C) generando $n=1000$ datos de la distribución 
# $abs(\mathcal{N}(\mu=0, \sigma=2)$ donde abs significa valor absoluto.
